const express = require('express');
const router = express.Router();
// const UserController = require('../controllers/user');//user change into course
const CourseController = require('../controllers/course');//user change into course
const auth = require('../auth');

// create a router
// create a controller logic

router.post('/', (req, res) => {
	CourseController.add(req.body).then(result => {
		res.send(result)
	})
})

router.get('/',(req, res) => {
	CourseController.getAll().then(courses => res.send(courses))
})

router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId
	CourseController.get({ courseId }).then(course => res.send(course))
})

router.put('/', auth.verify, (req, res) => {
	CourseController.update(req.body).then(result => res.send(result))
})

router.delete('/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId
	CourseController.archive({ courseId }).then(result => res.send(
		result))
})

module.exports = router;

// req.params - url
// archive